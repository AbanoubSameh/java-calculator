package calculator;

final class SizeCounter {

	// used to call the other countSize down there various times and sum up the
	// result
	static int countSize(String input) throws Exception {

		// sums up the sizes
		int count = countSize(input, "+") + countSize(input, "*") + countSize(input, "/");

		return count; // returns the total size, doesn't accommodate for the space needed to store
						// the 0 index or the length of the whole string so they need to be added
						// elsewhere

	}

	// used to count the of signIndex, needed to store all indexes of a string that
	// contains signs
	private static int countSize(String input, String sign) throws Exception {

		int size = 0;

		// finds another sign, goes ahead and increments by one
		while (input.contains(sign)) {

			// ** removes the part that has been checked **
			input = input.substring(input.indexOf(sign) + 1, input.length());
			size++;

		}

		// returns size
		return size;
	}

}
