package calculator;

final class Splitter {

	// splits the string into an array of numbers and signs
	static void splitInput(String input, Integer[] signIndex, String[] output) throws Exception {

		// VERY COMPLICATED STUFF, NOT TO BE MESSED WITH

		int first = 0, j = 1;

		// cycles throw, looks for the index in the signIndex array which contains
		// indexes of signs
		// and decides where to cut the string
		for (int i = 0; i < output.length - 1; i = i + 2) {

			output[i] = input.substring(first, signIndex[j]); // ** output takes the part before a given index **
			output[i + 1] = String.valueOf(input.charAt(signIndex[j])); // ** the sign in the given index **
			first = signIndex[j] + 1; // changes first and j to match the next cycle
			j++;

			// System.out.print(output[i] + " ");
			// System.out.print(output[i + 1] + " ");
		}

		// puts the length of the string at the end of the array, to be used in other
		// methods
		output[output.length - 1] = input.substring(signIndex[signIndex.length - 2] + 1, input.length());

		// System.out.println(output[output.length - 1]);
	}

}
