package calculator;

final class BracketSolver {

	// this is also one of the most complicated parts, it solves the brackets
	static String resolveBrackets(String input, String sign, String sign2) throws Exception {

		String answer;

		// String firstPart, secondPart;

		// solves the brackets as long as the opening one or the closing one exists
		while (input.contains(sign) || input.contains(sign2)) {

			// if exists only the opening one it adds the closing one
			// if exists only the closing one, too bad, can't do anything about it
			if (!input.substring(input.lastIndexOf(sign), input.length()).contains(sign2)) {
				input = input + sign2;
			}

			// System.out.println("input: " + input);
			// firstPart = input.substring(0, input.lastIndexOf(sign) + 1);
			// secondPart = input.substring(input.indexOf(sign2, input.lastIndexOf(sign)));

			// System.out.println(firstPart + " : " + secondPart);

			// substring of only the part inside the brackets
			answer = input.substring(input.lastIndexOf(sign) + 1, input.indexOf(sign2, input.lastIndexOf(sign)));

			// System.out.println("Doesn't have end");

			// System.out.println("This is you're answer: " + answer);

			// System.out.println("test char: " +
			// input.charAt(input.indexOf("(") - 1));

			// checks to see if brackets are next to each other or next to another number
			// without a sign
			// example: (x+y)(a+b) or a(b+c)
			if (input.lastIndexOf(sign) != 0 && input.charAt(input.lastIndexOf(sign) - 1) != '+'
					&& input.charAt(input.lastIndexOf(sign) - 1) != '-'
					&& input.charAt(input.lastIndexOf(sign) - 1) != '*'
					&& input.charAt(input.lastIndexOf(sign) - 1) != '/'
					&& input.charAt(input.lastIndexOf(sign) - 1) != '('
					&& input.charAt(input.lastIndexOf(sign) - 1) != '['
					&& input.charAt(input.lastIndexOf(sign) - 1) != '{') {

				// VERY COMPLICATED STUFF

				// it sends a substring back to calculator to cycle throw it all over again
				// to resolve operations and other brackets inside
				input = input.substring(0, input.lastIndexOf(sign)) + "*" + Calculator.calculate(answer)
						+ input.substring(input.indexOf(sign2, input.lastIndexOf(sign)) + 1, input.length());

			} else {

				// same thing here if there are no brackets next to each other or to another
				// number
				input = input.substring(0, input.lastIndexOf(sign)) + Calculator.calculate(answer)
						+ input.substring(input.indexOf(sign2, input.lastIndexOf(sign)) + 1, input.length());

			}

			// System.out.println("Output: " + input);

			// String ans = input.substring(0, input.indexOf("(")) + +
			// input.substring(input.indexOf(")") + 1, input.length());

		}

		// System.out.println(answer);

		return input; // returns the input after it has been manipulated
	}

}
