package calculator;

final class IndexCalculator {

	static int i = 0;

	// just used to call the other calculate index down there times instead of
	// having to call it manually
	static void calculateIndex(String input, Integer[] signIndex) throws Exception {

		calculateIndex(input, "+", signIndex); // calls it once for + signs
		// calculateIndex(input, "-", signIndex);
		calculateIndex(input, "*", signIndex); // once for * signs
		calculateIndex(input, "/", signIndex); // another time for / signs

		signIndex[signIndex.length - 1] = 0;
		signIndex[signIndex.length - 2] = input.length();

	}

	private static void calculateIndex(String input, String signX, Integer[] signIndex) throws Exception {

		// fairly complicated, used to calculate the indexes of a given sign
		int index = input.indexOf(signX);

		// cycles throw, checks to see every occurrence of the given sign
		while (index >= 0) {

			signIndex[i] = index; // found a sign, so adds it to sign index
			index = input.indexOf(signX, index + 1); // removes the part that has been checked
			i++;

		}

	}

}
