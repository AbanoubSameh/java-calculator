package calculator;

import java.util.Arrays;

final class NumberSolver {

	// this method is used to solve the operations one by one, the sign is given
	// in order to know which one we are dealing with
	static String resolveNumbers(String input, String sign) throws Exception {

		int size = SizeCounter.countSize(input); // calls a method to calculate size
		Integer[] signIndex = new Integer[size + 2]; // makes an array used to store indexes
		String[] output = new String[size * 2 + 1]; // ** string array used to store numbers and signs **

		IndexCalculator.calculateIndex(input, signIndex); // calculate indexes of signs and numbers
		Arrays.sort(signIndex); // sorts the array in ascending order, includes 0
								// and the overall length of the string(last index)

		Splitter.splitInput(input, signIndex, output); // splits the string in parts containing numbers or signs
														// based on indexes given

		while (Arrays.asList(output).contains(sign)) { // adds, multiplies, divides ...... until the sign of the
														// operation no longer exists

			switch (sign) { // used to know which operation we are dealing with, based on passed sign
			/*
			 * case "(": output[Arrays.asList(output).indexOf(sign) + 1] = String
			 * .valueOf(Integer.parseInt(output[Arrays.asList(output).indexOf( sign) - 1]) +
			 * Integer.parseInt(output[Arrays.asList(output).indexOf(sign) + 1])); break;
			 * 
			 */

			// THE VERY COMPLICATED STUFF, NOT TO BE MESSED WITH, THAT IS THE HEART OF THE
			// WHOLE THING

			// used to solve addition, also does subtraction with the inverse of the number
			case "+":

				// basically divides the string before the sign(+ in this case) from the string
				// after the sum
				// and turns it into doubles, then sums everything up
				output[Arrays.asList(output).indexOf(sign) + 1] = String
						.valueOf(Double.parseDouble(output[Arrays.asList(output).indexOf(sign) - 1])
								+ Double.parseDouble(output[Arrays.asList(output).indexOf(sign) + 1]));
				break;
			/*
			 * case "-": System.out.println("The first num: " +
			 * Integer.parseInt(output[Arrays.asList(output).indexOf(sign) - 1]));
			 * System.out.println("The sec num: " +
			 * Integer.parseInt(output[Arrays.asList(output).indexOf(sign) + 1]));
			 * 
			 * output[Arrays.asList(output).indexOf(sign) + 1] = String
			 * .valueOf(Integer.parseInt(output[Arrays.asList(output).indexOf( sign) - 1]) -
			 * Integer.parseInt(output[Arrays.asList(output).indexOf(sign) + 1])); break;
			 */

			// multiplication
			case "*":

				// does the same thing as before but multiplies
				output[Arrays.asList(output).indexOf(sign) + 1] = String
						.valueOf(Double.parseDouble(output[Arrays.asList(output).indexOf(sign) - 1])
								* Double.parseDouble(output[Arrays.asList(output).indexOf(sign) + 1]));
				break;

			// division
			case "/":

				// same thing here but divides
				output[Arrays.asList(output).indexOf(sign) + 1] = String
						.valueOf(Double.parseDouble(output[Arrays.asList(output).indexOf(sign) - 1])
								/ Double.parseDouble(output[Arrays.asList(output).indexOf(sign) + 1]));
				break;
			}

			// System.out.println(output[Arrays.asList(output).indexOf(sign) + 1]);

			output[Arrays.asList(output).indexOf(sign) - 1] = ""; // frees up output right before the sign
			output[Arrays.asList(output).indexOf(sign)] = ""; // same thing, frees up output sign
			IndexCalculator.i = 0; // increases index calculator by one

		}

		// System.out.println("This is the input: " + input);
		return printResult(output); // returns answer

	}

	// ** adds some string together basically
	private static String printResult(String[] input) throws Exception {

		String fin = "";

		for (int i = 0; i < input.length; i++) {

			fin = fin + input[i];

		}

		return fin;

	}

}
