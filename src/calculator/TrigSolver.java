package calculator;

public class TrigSolver {

	public static void main(String[] args) {

		solveTrig("sin(30");

		// System.out.println(Math.sin(Math.toRadians(0.499999999999)));

	}

	public static String solveTrig(String input) {

		String trigFunc = "sin(";

		while (input.contains("sin(") || input.contains("cos(") || input.contains("tan(") || input.contains("sec(")
				|| input.contains("cosec(") || input.contains("cotan(")) {

			if (!input.contains(")")) {

				input += ")";

			}

			String answer = "", temp = input.substring(input.lastIndexOf(trigFunc),
					input.substring(input.indexOf(trigFunc)).indexOf(")") + 1),
					rep = input.substring(input.lastIndexOf("(") + 1, input.indexOf(")"));

			// System.out.println("INPUT: " + input);
			// System.out.println("REP: " + rep);
			// System.out.println("TEMP: " + temp);

			switch (trigFunc) {

			case ("sin("):
				answer = String.valueOf(Math.sin(Math.toRadians(Double.parseDouble(rep))));
				break;

			case ("cos("):
				answer = String.valueOf(Math.cos(Math.toRadians(Double.parseDouble(rep))));
				break;

			case ("tan("):
				answer = String.valueOf(Math.tan(Math.toRadians(Double.parseDouble(rep))));
				break;

			}
			
			input = input.replace(temp, answer);

		}

		System.out.println(input);
		return input;

	}

}
