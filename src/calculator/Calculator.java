package calculator;

public final class Calculator {
	
	private Calculator() {}

	public static String calculate(String input) { // this method can also be called from the bracket resolving method
													// to solve brackets inside brackets
													// and other operations inside brackets

		input = input.replaceAll("//s+", "").replaceAll("-", "+-"); // replaces all + with +- to help during subtraction
																	// and also ** trims all white space **
																	// and maybe more

		try {

			// if input has a trigonometric function then it needs to be solved by TrigSolver 
			if(input.contains("sin(") || input.contains("cos(") || input.contains("tan(")
					|| input.contains("sec(") || input.contains("cosec(") || input.contains("cotan(")){
				
				input = TrigSolver.solveTrig(input);
				
			}
			
			// if the input has the sign then it will send it to the right method to be solved
			if (input.contains("+") || input.contains("-") || input.contains("*") || input.contains("/")
					|| input.contains("(") || input.contains(")") || input.contains("{") || input.contains("}")
					|| input.contains("[") || input.contains("]")){

				// first it solves these brackets always from right to left
				if (input.contains("(") || input.contains(")")) {
					input = BracketSolver.resolveBrackets(input, "(", ")");
					// System.out.println(input);
				}

				// then the other brackets
				if (input.contains("[") || input.contains("]")) {
					input = BracketSolver.resolveBrackets(input, "[", "]");
					// System.out.println(input);
				}

				// then these ones
				if (input.contains("{") || input.contains("}")) {
					input = BracketSolver.resolveBrackets(input, "{", "}");
					// System.out.println(input);
				}

				// next we begin with the division
				if (input.contains("/")) {
					input = NumberSolver.resolveNumbers(input, "/");
					// System.out.println(input);
				}

				// multiplication
				if (input.contains("*")) {
					input = NumberSolver.resolveNumbers(input, "*");
					// System.out.println(input);
				}

				// addition, subtraction is also done here but with the inverse of the number
				if (input.contains("+")) {
					input = NumberSolver.resolveNumbers(input, "+");
					// System.out.println(input);
				}
				/*
				 * if (input.contains("-")) { input = finish(input, "-"); }
				 */
			}

			// ** all exceptions should be thrown and handled here **
		} catch (Exception e) {

			System.out.println("Error");
			System.out.println(e.getMessage());
			return "";

		}

		// lastly we return the input after manipulating it
		return input;

	}

}