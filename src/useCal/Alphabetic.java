package useCal;

public class Alphabetic {
	
	public static String solve(String input, char sign) {
		
		int j1 = 0, j2 = 0;

		String first = "", second = "";

		while (input.contains(String.valueOf(sign))) {

			for (int i = 0; i < input.length(); i++) {

				if (input.charAt(i) == sign) {

					// System.out.println(i);

					for (int j = i - 1; j >= 0; j--) {

						// System.out.println(!Character.isDigit(input.charAt(j)));

						if (!Character.isDigit(input.charAt(j)) && input.charAt(j) != '.') {

							first = input.substring(j + 1, i);
						//	System.out.println(first);
							j1 = j + 1;
							break;

						} else if (j == 0) {

							first = input.substring(j, i);
						//	System.out.println(first);
							j1 = j;
							break;

						}

					}

					for (int j = i + 1; j < input.length(); j++) {

						if (!Character.isDigit(input.charAt(j)) || j == input.length() - 1) {

							second = input.substring(i + 1, j);
						//	System.out.println(second);
							j2 = j;
							break;

						}

					}

				//	System.out.println("First: " + first);
				//	System.out.println("Second: " + second);

					switch (sign) {

					case '+':
						input = input.substring(0, j1)
								+ String.valueOf(Double.parseDouble(first) + Double.parseDouble(second))
								+ input.substring(j2);
					//	System.out.println("Here's your input: " + input);
						break;

					case '-':
						input = input.substring(0, j1)
								+ String.valueOf(Double.parseDouble(first) - Double.parseDouble(second))
								+ input.substring(j2);
					//	System.out.println("Here's your input: " + input);
						break;

					case '*':
						input = input.substring(0, j1)
								+ String.valueOf(Double.parseDouble(first) * Double.parseDouble(second))
								+ input.substring(j2);
					//	System.out.println("Here's your input: " + input);
						break;

					case '/':
						input = input.substring(0, j1)
								+ String.valueOf(Double.parseDouble(first) / Double.parseDouble(second))
								+ input.substring(j2);
					//	System.out.println("Here's your input: " + input);
						break;

					}

				}

			}

			// System.out.println(answer);

		}

		return input;
		
	}

}
